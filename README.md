# Wallpapers

## Description
Some of my favorite wallpapers. Currently running triple 32 inch 4k monitors all in landscape mode on the desktop so some are made for three screens.

## Contributing
Pulled from various places around the web

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
I will be updating new wallpapers as I find and use them. 
